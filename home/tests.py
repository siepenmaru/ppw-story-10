from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import index, register
from .apps import HomeConfig

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exists(self): # checks if url exists
        response = Client().get('/',follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_index_function(self): # checks if index function is used
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_register_function(self): # checks if register function is used
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_header_index_correct(self): # tests if header content is correct
        response = Client().get('/',follow=True)
        response_content = response.content.decode('utf-8')
        self.assertIn("<h1>Avatar's Story 10 Website</h1>", response_content)

    def test_goes_to_index(self): # tests if url goes to index html
        response = Client().get('/',follow=True)
        self.assertTemplateUsed(response, 'home/index.html')

    def test_goes_to_register(self): # tests if url goes to register html
        response = Client().get('/register',follow=True)
        self.assertTemplateUsed(response, 'home/register.html')

    def test_app_success(self): # tests if app is used
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')

    def test_forms_success(self): # tests valid form is submitted
        response = self.client.post('/register/', follow = True, data = {'username':'Lorem','password1':'Password101','password2':'Password101'})
        self.assertEqual(response.status_code, 200)