from django.contrib import admin
from django.urls import include, path
from home import views

urlpatterns = [
    path('register/', views.register, name="home-register"),
    path('', views.index, name="home-index"),
]